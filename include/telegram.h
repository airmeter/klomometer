#ifndef __TELEGRAM_H
#define __TELEGRAM_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "data.h"

class TELEGRAM {
private:
public:
  TELEGRAM() {}
  TELEGRAM(struct data* _data, struct tm* _time);//float* _temp, uint8_t* _humid, bool* _shower, bool* _window_open, struct tm* _time);
  void SendMessage(uint8_t id, char txt[]);
  void SendMessage(String id, char txt[]);
  void handleNewMessages(int numNewMessages);
  void CheckMessages();
};
#endif
