#ifndef __WIFI_FUN_H
#define __WIFI_FUN_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <data.h>

class WIFI {
private:
public:
  WIFI() {}
  WIFI(struct data* _data);//float* _temp, uint8_t* _humid, bool* _shower, bool* _window_open);
  uint8_t Send_New_Data();
  void timeupdate();
  struct tm* Get_Time();
  uint64_t getEpochTime();
  void update();
};
#endif
