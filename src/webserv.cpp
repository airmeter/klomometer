#include "webserv.h"
#include <ESPAsyncWebServer.h> // https://github.com/me-no-dev/ESPAsyncWebServer/archive/master.zip und https://github.com/me-no-dev/AsyncTCP/archive/master.zip
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESP8266mDNS.h>
#include "webpages.h"
#include <esp8266httpclient.h>
#include <ArduinoJson.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "personal_data.h"

void HandleNotFound(AsyncWebServerRequest* request);
void HandleReboot(AsyncWebServerRequest* request);
void HandleRoot(AsyncWebServerRequest* request);
void HandleGetData(AsyncWebServerRequest* request);

AsyncWebServer server(80);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 3600);

struct data* wifi_data;
/*float* wifi_temperature;
uint8_t* wifi_humidity;
bool* wifi_shower;
bool* wifi_window_open;*/

void HandleGetData(AsyncWebServerRequest* request) {
  char status[26];
  sprintf(status, "%4.1f;%u", wifi_data->temperature, wifi_data->humidity);
  request->send(200, "text/html", status);
}

void HandleRoot(AsyncWebServerRequest* request) {
  char wp[strlen(mainpage)];
  sprintf(wp, mainpage, wifi_data->temperature, wifi_data->humidity);
  request->send(200, "text/html", wp);
}

void HandleReboot(AsyncWebServerRequest* request) {
  Serial.println("Restarting...");
  ESP.restart();
}

void HandleNotFound(AsyncWebServerRequest* request)
{
  request->redirect("/?error=notfound");
}

WIFI::WIFI(struct data* _data) { //float* _temp, uint8_t* _humid, bool* _shower, bool* _window_open) {
  wifi_data = _data;
  /*  wifi_temperature = _temp;
    wifi_humidity = _humid;
    wifi_shower = _shower;
    wifi_window_open = _window_open;*/

  Serial.printf("Connecting with WIFI: '%s'\n", SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PASS);

  Serial.printf("Connecting.");

  uint8_t i = 0;
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    i++;
    if (i % 40 == 5) { // 2s gone -> 2nd try
      Serial.printf("\nCould not connect to WIFI! Resetting WiFi-Connecting and try one more time\n");
      WiFi.disconnect(true);
      delay(1000);
      WiFi.begin(SSID, PASS);
    }
  }
  /*
    // Begin OTA-Update
    ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
      });
    ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
      });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      });
    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
      });
    ArduinoOTA.begin();
    // End OTA-Update
  */
  server.onNotFound(HandleNotFound);
  server.on("/", HTTP_GET, HandleRoot);
  server.on("/getdata", HTTP_GET, HandleGetData);
  server.on("/reboot", HTTP_GET, HandleReboot);
  server.on("/reset_shower", HTTP_GET, [](AsyncWebServerRequest* request) {
    wifi_data->shower = 0;
    request->send(200, "text/html", "");
    Serial.printf("Dusche zurueckgesetzt\n");
    });
  server.on("/reset_window", HTTP_GET, [](AsyncWebServerRequest* request) {
    wifi_data->window_open = 0;
    request->send(200, "text/html", "");
    Serial.printf("Fenster zurueckgesetzt\n");
    });

  server.begin();

  Serial.printf("Wifi verbunden!\n");

  if (MDNS.begin("klomometer")) {
    Serial.printf("Klomometer zugreifbar über: klomometer.local\n");
  }

  Serial.print("\nIP number assigned by DHCP is ");
  Serial.println(WiFi.localIP());


  while (1) {
    timeClient.begin();
    timeClient.update();
    delay(10000);
    struct tm* time = Get_Time();
    if (time->tm_year > 120)
      break;

    Serial.printf("Failed getting time! (%02u.%02u.%u %02u:%02u:%02u)\n", time->tm_mday, time->tm_mon + 1, time->tm_year + 1900, time->tm_hour, time->tm_min, time->tm_sec);
    timeClient.end();
  }
}

uint8_t WIFI::Send_New_Data() {
  char url[200];
  struct tm* ptm = Get_Time();
  sprintf(url, "http://klomometer.marmeladenkekse.de/send.php?date=%u-%02u-%02u&time=%02u:%02u:%02u&temp=%d&humi=%u&shower=%c&window_open=%c", 1900 + ptm->tm_year, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int16_t)(wifi_data->temperature * 10), (uint16_t)(wifi_data->humidity), wifi_data->shower ? '1' : '0', wifi_data->window_open ? '1' : '0');
  HTTPClient http;
  http.begin(url);
  http.addHeader("Content-Type", "text/plain");
  int httpCode = http.GET();
  if (httpCode != 200) {
    Serial.printf("Failed sending data!\n");
  }
  http.end();

  return 0;
}

struct tm* WIFI::Get_Time() {
  unsigned long epochTime = timeClient.getEpochTime();
  struct tm* ptm = gmtime((time_t*)&epochTime);
  return ptm;
}

uint64_t WIFI::getEpochTime() {
  return timeClient.getEpochTime();
}

void WIFI::timeupdate() {
  timeClient.update();
}

void WIFI::update() {
  ArduinoOTA.handle();
}