#include "DHT.h"
#include "webserv.h"
#include "telegram.h"
#include "IntervalFunctions.h"

#define DHTPIN  D2      // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)

DHT dht(DHTPIN, DHTTYPE);
WIFI wifi;

TELEGRAM telegram;

//float* temperature;
//uint8_t* humidity;

const uint8_t WINDOW_PIN = 5; // D1
//bool window_open = false;
uint32_t window_open_time = 0;
uint8_t window_open_minimal_temp = -1;
//bool shower = false;
uint32_t shower_time = 0;
uint8_t shower_maximal_humi = 0;
bool humidity_high_warning_send = false;

// In ein Struct packen und an telegram.cpp übergeben, damit telgram was an den Parametern schrauben kann
uint8_t TEMP_DIFF_WINDOW_OPENED = 5;      // Temperaturdifferenz in °C wenn das Fenster geöffnet wurde
uint8_t HUMI_DIFF_WINDOW_OPENED = 5;      // Feuchtigkeitsdifferenz in % wenn das Fenster geöffnet wurde
uint8_t TEMP_DIFF_WINDOW_CLOSED = 1;      // Temperaturdifferenz in °C wenn das Fenster geschlossen wurde
uint32_t WINDOW_OPEN_WARNING_TIME = 300;  // Zeit in Sekunden nach der nach dem Fenster öffnen gewarnt werden soll, dass das Fenster noch offen ist
uint8_t HUMI_DIFF_SHOWER_BEGIN = 5;       // Feuchtigkeitsdifferenz in % wenn das Duschen begonnen wurde
uint8_t HUMI_DIFF_SHOWER_END = 5;         // Feuchtigkeitsdifferenz in % wenn das Duschen beendet wurde
uint8_t HUMI_HIGH = 60;                   // Luftfeuchtigkeit ab der gewarntet werden soll, dass gelüftet werden muss
uint32_t HUMI_HIGH_WARNING_TIME = 600;    // Zeit in Sekunden nach dem Duschen gewarnt werden soll, dass gelüftet werden soll
uint32_t TEMP_LOW = 18;

//struct tm* datetime;
struct data data;
struct tm* datetime;

IntervalFunctions intervalfunctions;

void Measure_Values();
const uint32_t Measure_Values_Time = 60000UL;

void TimeUpdate() {
  wifi.timeupdate();
}
const uint32_t TimeUpdate_Time = 3600000UL;

void TelegramStatusUpdate();
const uint32_t Telegram_Status_Update_Time = 3600UL * 1000UL;// *24;

void CheckTelegramMessages() {
  telegram.CheckMessages();
}
const uint32_t Telegram_Check_Messages = 1000;

ICACHE_RAM_ATTR void Window_Changed() {
  if (digitalRead(WINDOW_PIN)) {
    data.window_open = true;
    window_open_time = -1;
    Serial.println("Openend");
  }
  else {
    data.window_open = false;
    Serial.println("Closed");
  }
}

void setup() {
  Serial.begin(500000);
  //temperature = (float*)malloc(sizeof(float));
  //humidity = (uint8_t*)malloc(sizeof(uint8_t));
  data.temperature = 0;
  data.humidity = 0;

  Serial.println("Initialize DHT...");
  dht.begin();
  Serial.println("DHT initialized!");

  wifi = WIFI(&data);//temperature, humidity, & shower, & window_open);
  datetime = wifi.Get_Time();
  data.datetime = *datetime;
  telegram = TELEGRAM(&data, datetime);//temperature, humidity, & shower, & window_open, datetime);
  telegram.SendMessage(0, "Klomometer online!");

  uint8_t h;
  float t;
  while (1) {
    h = (uint8_t)dht.readHumidity();
    t = dht.readTemperature();
    if (!(isnan(h) || isnan(t)))
      break;
    delay(2000);
  }
  data.temperature = t;
  data.humidity = h;

  delay(2000);

  intervalfunctions = IntervalFunctions(10);
  intervalfunctions.AddFunction(Measure_Values_Time, Measure_Values, Enabled);
  intervalfunctions.AddFunction(TimeUpdate_Time, TimeUpdate, Enabled);
  intervalfunctions.AddFunction(Telegram_Status_Update_Time, TelegramStatusUpdate, Enabled);
  intervalfunctions.AddFunction(Telegram_Check_Messages, CheckTelegramMessages, Enabled);

  pinMode(WINDOW_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(WINDOW_PIN), Window_Changed, CHANGE);

  Serial.printf("Initialization done!\n");
}

void loop() {
  intervalfunctions.run();
  wifi.update();
}

void Measure_Values() {
  Serial.print("Measure values...\n");
  uint8_t h = (uint8_t)dht.readHumidity();
  float t = dht.readTemperature();
  char message_txt[200];
  uint32_t current_time = wifi.getEpochTime();

  if (isnan(h) || isnan(t)) {
    sprintf(message_txt, "%02u.%02u.%u %02u:%02u:%02u: DHT-Sensor konnte nicht ausgelesen werden!", data.datetime.tm_mday, data.datetime.tm_mon + 1, data.datetime.tm_year + 1900, data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec);
    Serial.println(message_txt);
    telegram.SendMessage(0, message_txt);
  }
  else {
    Serial.printf("Values measured:\nTemperature: %4.1f C\nHumidity: %u %%\n", t, h);

    if (data.shower) {
      if (h > shower_maximal_humi)
        shower_maximal_humi = h;
      if (h < shower_maximal_humi - HUMI_DIFF_SHOWER_END) { // Feuchtigkeit gesunken -> Duschen beendet
        data.shower = false;
        shower_maximal_humi = 0;
        Serial.printf("%02u:%02u:%02u: Feuchtigkeit gesunken -> Duschen beendet\n", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec);
      }
    }
    else {
      if (h > data.humidity + HUMI_DIFF_SHOWER_BEGIN) { // Feuchtigkeit gestiegen -> Duschen gestartet
        data.shower = true;
        shower_time = current_time;
        Serial.printf("%02u:%02u:%02u: Feuchtigkeit gestiegen -> Duschen gestartet\n", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec);
      }
    }

    if (data.window_open) {
      if (window_open_time == (uint32_t)-1) {
        window_open_time = current_time;
      }
      /*if (t < window_open_minimal_temp)
        window_open_minimal_temp = t;
      if (t > window_open_minimal_temp + TEMP_DIFF_WINDOW_CLOSED) { // Temperatur gestiegen -> Fenster geschlossen
        window_open = false;
        window_open_minimal_temp = -1;
        Serial.printf("%02u:%02u:%02u: Temperatur gestiegen -> Fenster geschlossen\n", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec);
      }*/
      if (t < TEMP_LOW && current_time - window_open_time > WINDOW_OPEN_WARNING_TIME) { // Fenster zu lange geöffnet -> Warnung absenden
        sprintf(message_txt, "%02u:%02u:%02u: Mach das Fenster zu, es wird kalt!\nTemperatur: %f °C\nLuftfeuchtigkeit: %u %%", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec, t, h);
        telegram.SendMessage(1, message_txt); // Minütliche Warnung
        Serial.println(message_txt);
      }
    }
    else {
      /*if (t < *temperature - TEMP_DIFF_WINDOW_OPENED || h < *humidity - HUMI_DIFF_WINDOW_OPENED) { // Temperatur gefallen -> Fenster geöffnet
        window_open = true;
        window_open_time = current_time;
        Serial.printf("%02u:%02u:%02u: Temperatur gefallen -> Fenster geöffnet\n", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec);
      }
      if (shower && current_time - shower_time > HUMI_HIGH_WARNING_TIME) { // Duschen beendet, Fenster geschlossen -> Warnung absenden
        sprintf(message_txt, "%02u:%02u:%02u: Mach das Fenster auf, du hast geduscht!\nTemperatur: %f °C\nLuftfeuchtigkeit: %u %%", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec, t, h);
        telegram.SendMessage(message_txt); // Minütliche Warnung
        Serial.println(message_txt);
      }*/
    }

    // Allgemeine Warnung vor zu hoher Luftfeuchtigkeit
    if (h > HUMI_HIGH && !humidity_high_warning_send && !data.shower && current_time - shower_time > HUMI_HIGH_WARNING_TIME) { // Hohe Luftfeuchtigkeit, nicht am Duschen, noch keine Warnung gesendet -> Warnung absenden
      humidity_high_warning_send = true;
      sprintf(message_txt, "%02u:%02u:%02u: Die Luftfeuchtigkeit ist hoch, du solltest lüften!\nTemperatur: %f °C\nLuftfeuchtigkeit: %u %%", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec, t, h);
      telegram.SendMessage(1, message_txt); // Einmalige Warnung
      Serial.println(message_txt);
    }
    if (humidity_high_warning_send && h < HUMI_HIGH) {
      humidity_high_warning_send = false;
      Serial.printf("%02u:%02u:%02u: Feuchtigkeit unter Normalwert gesunken\n", data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec);
    }

    data.temperature = t;
    data.humidity = h;

    if (wifi.Send_New_Data()) {
      Serial.printf("Sending failed!\n");
    }
  }
}

void TelegramStatusUpdate() {
  char txt[300];
  sprintf(txt, "Klomometer meldet sich weiterhin online! Wir haben den %02u.%02u.%u und es ist %02u:%02u:%02u. Es sind %2.1f °C und wir haben eine Luftfeuchtigkeit von %u %%. Es wird %sund %s.", data.datetime.tm_mday, data.datetime.tm_mon + 1, data.datetime.tm_year + 1900, data.datetime.tm_hour, data.datetime.tm_min, data.datetime.tm_sec, data.temperature, data.humidity, data.shower ? "aktuell geduscht " : "aktuell nicht geduscht ", data.window_open ? "das Fenster ist offen" : "das Fenster ist zu");
  telegram.SendMessage(0, txt);
}