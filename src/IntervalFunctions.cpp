#include "IntervalFunctions.h"
#include <stdlib.h>
#include <Arduino.h>
//#include <WProgram.h>

IntervalFunctions::IntervalFunctions(uint8_t amount)
{
    this->functions = (IvF*)malloc(amount * sizeof(IvF));
    this->amount = amount;
    for (uint8_t i = 0; i < amount; i++)
    {
        this->functions[i].used = 0;
    }
}

uint8_t IntervalFunctions::AddFunction(void (*f)())
{
    return this->AddFunction(-1, f);
}

uint8_t IntervalFunctions::AddFunction(uint32_t interval, void (*f)(), Function_Enabled fe) {
    uint8_t i = this->AddFunction(interval, f);
    if (fe == Enabled)
        this->enable(i);
    return i;
}

uint8_t IntervalFunctions::AddFunction(uint32_t interval, void (*f)())
{
    uint8_t i = 0;
    while (this->functions[i].used && i < this->amount)
        i++;

    if (i == amount)
        return -1;

    this->functions[i].f = f;
    this->functions[i].interval = interval;
    this->functions[i].last = millis();
    this->functions[i].enabled = 0;
    this->functions[i].used = 1;
    return i;
}

void IvF::enable()
{
    this->enabled = 1;
    this->last = millis();
}

void IvF::disable()
{
    this->enabled = 0;
}

void IntervalFunctions::enable(uint8_t i)
{
    this->functions[i].enable();
}

void IntervalFunctions::disable(uint8_t i)
{
    this->functions[i].disable();
}

uint8_t IntervalFunctions::run()
{    
    uint32_t t = millis();
    uint8_t counter = 0;

    for (uint8_t i = 0; i < this->amount; i++)
    {
        if (this->functions[i].used && this->functions[i].enabled)
        {
            if (t - this->functions[i].last > this->functions[i].interval)
            {
                this->functions[i].f();
                this->functions[i].last = t;
                counter++;
            }
        }
    }

    return counter;
}

uint8_t IntervalFunctions::enabled(uint8_t i)
{
    return this->functions[i].enabled;
}

void IntervalFunctions::Interval(uint8_t i, uint32_t value)
{
    this->functions[i].interval = value;
}