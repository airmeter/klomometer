#ifndef _INTERVALFUNCTION_H
#define _INTERVALFUNCTION_H

#include <stdint.h>
class IvF
{
public:
  void enable();
  void disable();

  void (*f)();
  uint32_t interval;
  uint32_t last;
  uint8_t used;
  uint8_t enabled;
};

typedef enum {
  Enabled = 1,
  Disabled
} Function_Enabled;

class IntervalFunctions
{
public:
  IntervalFunctions() {}
  IntervalFunctions(uint8_t amount);
  uint8_t AddFunction(void (*f)());
  uint8_t AddFunction(uint32_t interval, void (*f)());
  uint8_t AddFunction(uint32_t interval, void (*f)(), Function_Enabled fe);
  uint8_t run();
  void enable(uint8_t i);
  uint8_t enabled(uint8_t i);
  void disable(uint8_t i);
  void Interval(uint8_t i, uint32_t value);

private:
  IvF* functions;
  uint8_t amount;
};
#endif
